import { firestore } from "firebase-admin"
import { Match, MATCHES_COLLECTION } from "../src/app/match/match.service"
import { Vote, VOTES_COLLECTION } from "../src/app/match/vote.service"
import { init } from "./shared"

async function getMatches() {
  const f = firestore()

  const matchCol = f.collection(MATCHES_COLLECTION) as firestore.CollectionReference<Match>
  
  return (await matchCol.get()).docs
}

async function getVotes(match: firestore.QueryDocumentSnapshot<Match>) {
  const votesCol = match.ref.collection(VOTES_COLLECTION) as firestore.CollectionReference<Vote>

  return (await votesCol.get()).docs
}

async function getVotesByUID(matches: firestore.QueryDocumentSnapshot<Match>[]) {
  const votesByUID: {[uid: string]: string[]} = {}
  
  for (let m of matches) {
    const votes = await getVotes(m)

    votes.forEach(v => {
      if (votesByUID[v.id] === undefined) {
        votesByUID[v.id] = []
      }
      votesByUID[v.id][parseInt(m.id, 10)] = v.data().votedFor.id
    })
  }

  return votesByUID
}

function printVotes(votes: { [uid: string]: string[] }) {
  let str = `,`
  for (let i = 1; i <= 52; ++i) {
    str += `${i},`
  }
  console.log(str)

  Object.keys(votes).forEach(id => {
    let str = `${id},`
    for (let i = 1; i <= 52; ++i) {
      str += `${votes[id][i] || ''},`
    }
    console.log(str)
  })
}


async function main() {
  init()

  const matches = await getMatches()

  const votesByUID = await getVotesByUID(matches)

  printVotes(votesByUID)
}

main()
