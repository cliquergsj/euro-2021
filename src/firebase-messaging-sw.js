// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.6.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.

firebase.initializeApp({
  apiKey: "AIzaSyBF-e8bJgeXfGb2Dxw_EM546tHEaJfZAgI",
  authDomain: "clique-rgsj.firebaseapp.com",
  databaseURL: "https://clique-rgsj-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "clique-rgsj",
  storageBucket: "clique-rgsj.appspot.com",
  messagingSenderId: "822882469840",
  appId: "1:822882469840:web:ae120b51db97201ca6dbd5",
  measurementId: "G-SC9ZZQ7PXK"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
