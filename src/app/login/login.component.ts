import { Component, AfterViewInit } from '@angular/core';
import { FirebaseError } from '@angular/fire/app';
import { Auth, AuthErrorCodes, getRedirectResult, OAuthProvider, PhoneAuthProvider, RecaptchaVerifier, signInWithCredential, signInWithRedirect } from '@angular/fire/auth';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewInit {

  phone: string = ''
  get formatedPhone(): string {
    return `+32${this.phone.replace(/\D/g, '')}`
  }
  private phoneRecaptchaVerifier!: RecaptchaVerifier

  verificationCode: string = ''
  verificationId: string = ''

  processing = true

  error = ''

  constructor(private auth: Auth,
      private router: Router,
      private activeRoute: ActivatedRoute) {
  }

  ngAfterViewInit(): void {
    this.processing = true

    // setTimeout(() => {
    //   this.initCaptcha()
    // }, 100)
    this.loginMyClique()
  }

  private initCaptcha() {
    try {
      // Somehow firebase.auth is undefined for a moment
      this.phoneRecaptchaVerifier = new RecaptchaVerifier('recaptcha-container', {
        size: 'invisible'
      }, this.auth);

      this.processing = false
    } catch {
      setTimeout(() => this.initCaptcha(), 100)
    }
  }

  async loginMyClique() {
    const result = await getRedirectResult(this.auth);
    if (result) {
      this.activeRoute.queryParams.subscribe(({ from }) => {
        this.router.navigateByUrl(from)
      })

      return
    }

    const provider = new OAuthProvider('oidc.myclique');
    provider.addScope('profile');
    provider.addScope('email');

    await signInWithRedirect(this.auth, provider);
  }

  async login1() {
    if (this.processing) {
      return
    }
    try {
      this.processing = true
      this.error = ''

      const provider = new PhoneAuthProvider(this.auth)

      this.verificationId = await provider.verifyPhoneNumber(
        this.formatedPhone, this.phoneRecaptchaVerifier)

    } catch (err: unknown) {
      if (err instanceof FirebaseError) {
        switch (err.code) {
        case AuthErrorCodes.CAPTCHA_CHECK_FAILED:
          this.error = 'Erreur de vérification CAPTCHA'
          break
        case AuthErrorCodes.INVALID_PHONE_NUMBER:
        case AuthErrorCodes.MISSING_PHONE_NUMBER:
          this.error = 'Numéro de téléphone invalide'
          break
        case AuthErrorCodes.QUOTA_EXCEEDED:
          this.error = 'Quota de connexion dépassé'
          break
        case AuthErrorCodes.USER_DISABLED:
          this.error = 'Numéro de téléphone banni'
          break
        default:
          this.error = `Erreur inconnue: ${err.message}`
          break
        }
      } else {
        this.error = `Erreur inconnue: ${err}`
      }
    } finally {
      this.processing = false
    }
  }

  async login2() {
    if (this.processing) {
      return
    }
    try {
      this.processing = true
      this.error = ''

      const credential = PhoneAuthProvider.credential(this.verificationId, this.verificationCode)
      await signInWithCredential(this.auth, credential)

      this.activeRoute.queryParams.subscribe(({ from }) => {
        this.router.navigateByUrl(from)
      })
    } catch (err) {
      if (err instanceof FirebaseError) {
        switch (err.code) {
        case AuthErrorCodes.INVALID_CODE:
        case AuthErrorCodes.MISSING_CODE:
          this.error = 'Numéro de confirmation invalide'
          break
        default:
          this.error = `Erreur inconnue: ${err.message}`
          break
        }
      } else {
        this.error = `Erreur inconnue: ${err}`
      }
    } finally {
      this.processing = false
    }
  }

}
