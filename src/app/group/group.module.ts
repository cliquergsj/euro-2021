import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupComponent } from './group.component';
import { GroupTableModule } from '../group-table/group-table.module';
import { MatchesModule } from '../matches/matches.module';
import { MatDividerModule } from '@angular/material/divider';



@NgModule({
  declarations: [
    GroupComponent
  ],
  imports: [
    CommonModule,
    MatDividerModule,
    GroupTableModule,
    MatchesModule,
  ]
})
export class GroupModule { }
