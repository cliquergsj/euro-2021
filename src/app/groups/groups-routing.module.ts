import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupComponent } from '../group/group.component';
import { GroupNamesService } from './group-names.service';
import { GroupsComponent } from './groups.component';

const routes: Routes = [{
  path: '',
  component: GroupsComponent,
  children: [
    {
      path: ':group',
      component: GroupComponent,
      title: GroupNamesService,
    },
    { path: '', redirectTo: 'a', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupsRoutingModule { }
