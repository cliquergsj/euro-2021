import { TestBed } from '@angular/core/testing';

import { GroupNamesService } from './group-names.service';

describe('GroupNamesService', () => {
  let service: GroupNamesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GroupNamesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
