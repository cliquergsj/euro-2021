import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { GroupName } from 'functions/src/models/group';
import { GroupMatchType, KnockoutMatchType, MatchType } from 'functions/src/models/match';
import { Observable } from 'rxjs';
import { GroupNamesService } from '../groups/group-names.service';

@Injectable({
  providedIn: 'root'
})
export class MatchNameService implements Resolve<string> {

  constructor(private gn: GroupNamesService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
    const type = route.params.type
    if (typeof type === 'string') {
      return this.getLabel(type as MatchType)
    }
    return ''
  }

  getLabel(type: GroupMatchType, group?: GroupName): string
  getLabel(type: KnockoutMatchType): string
  getLabel(type: MatchType, group?: GroupName): string
  getLabel(type: MatchType, group?: GroupName): string {
    const groupPart = []
    if (group) {
      groupPart[0] = this.gn.getLabel(group)
    }
    switch (type) {
    case 'group1':
      return ['Phase de groupes', ...groupPart, 'Journée 1 sur 3'].join(' · ')
    case 'group2':
      return ['Phase de groupes', ...groupPart, 'Journée 2 sur 3'].join(' · ')
    case 'group3':
      return ['Phase de groupes', ...groupPart, 'Journée 3 sur 3'].join(' · ')
    case 'eighth':
      return '8e de finale'
    case 'quarter':
      return 'Quart de finale'
    case 'semi':
      return 'Demi-finale'
    case 'final':
      return 'Finale'
    }
  }
}
