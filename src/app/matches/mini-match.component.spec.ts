import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniMatchComponent } from './mini-match.component';

describe('MiniMatchComponent', () => {
  let component: MiniMatchComponent;
  let fixture: ComponentFixture<MiniMatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiniMatchComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MiniMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
