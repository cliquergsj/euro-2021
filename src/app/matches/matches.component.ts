import { Component, Inject, InjectionToken, Input, OnChanges, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupName } from 'functions/src/models/group';
import { Match, MatchType } from 'functions/src/models/match';
import { asyncScheduler, of } from 'rxjs';
import { Observable } from 'rxjs';
import { observeOn, tap } from 'rxjs/operators';
import { MatchService } from '../match-detail/match.service';

export type ListType = 'recent'

export const LIST_TOKEN = new InjectionToken<ListType>("list")
export const LIMIT_TOKEN = new InjectionToken<number>("limit")

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss']
})
export class MatchesComponent implements OnChanges {

  @Input() group?: GroupName = undefined
  @Input() type?: MatchType = undefined
  @Input() list?: ListType = undefined
  @Input() limit?: number = undefined

  loading = true

  matches$: Observable<Match[]> = of([])

  constructor(private service: MatchService,
    r: ActivatedRoute,
    router: Router,
    @Optional() @Inject(LIST_TOKEN) list?: ListType,
    @Optional() @Inject(LIMIT_TOKEN) limit?: number) {

    if (list) {
      this.list = list
      this.limit = limit
      this.ngOnChanges()
    }

    r.params
      .pipe(observeOn(asyncScheduler))
      .subscribe(o => {
        if (o.type === 'current') {
          router.navigate(['..', service.getCurrent()], {
            replaceUrl: true,
            relativeTo: r,
          })
        } else if (o.type) {
          this.type = o.type

          this.ngOnChanges()
        }
      })
  }

  ngOnChanges() {
    let matches$: Observable<Match[]> = of([])

    if (this.list) {
      switch (this.list) {
        case 'recent':
          matches$ = this.service.recentMatches$(this.limit!)
          break;
      }

    } else if (this.type) {
      matches$ = this.service.getMatchesByType$(this.type)
    } else if (this.group) {
      matches$ = this.service.getMatchesByGroup$(this.group)
    }

    this.matches$ = matches$.pipe(tap(_ => {
      // Workaround to avoid "NG0100: Expression has changed after it was checked"
      setTimeout(() => this.loading = false, 0)
    }))
  }

  showDate(list: Match[], i: number): boolean {
    if (i === 0) {
      return true
    }
    const thisDate = list[i].date
    const previousDate = list[i - 1].date
    return previousDate.toDate().getDate() !== thisDate.toDate().getDate()
  }

  trackBy(_: number, m: Match): any {
    return m.id
  }
}
