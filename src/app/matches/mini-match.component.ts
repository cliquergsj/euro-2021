import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatchComponent } from '../match-detail/match.component';

@Component({
  selector: 'app-mini-match',
  templateUrl: './mini-match.component.html',
  styleUrls: ['./mini-match.component.scss']
})
export class MiniMatchComponent extends MatchComponent {

  getName$(side: 'left' | 'right'): Observable<string> {
    const m = this.match
    const member = side === 'left' ? m.left : m.right
    const desc = side === 'left' ? m.leftDesc : m.rightDesc

    return this.morceauServ.getMorceau$(member).pipe(
      map(m => m?.name || desc)
    )
  }

}
