import { NgModule } from "@angular/core";
import { connectAuthEmulator, getAuth, provideAuth } from "@angular/fire/auth";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "src/environments/environment";
import { AppComponent } from "./app.component";
import { AppModule } from "./app.module";

@NgModule({
  imports: [
    AppModule,
    provideAuth(() => {
      const auth = getAuth()
      if (!environment.production) {
        connectAuthEmulator(auth, 'http://localhost:9099')
      }
      return auth
    }),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  bootstrap: [AppComponent],
})
export class AppBrowserModule {}
