import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MorceauAdminComponent } from './morceau-admin.component';

describe('MorceauAdminComponent', () => {
  let component: MorceauAdminComponent;
  let fixture: ComponentFixture<MorceauAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MorceauAdminComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MorceauAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
