import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Morceau } from 'functions/src/models/morceau';
import { Observable } from 'rxjs';
import { MorceauService } from '../morceau/morceau.service';

@Component({
  selector: 'app-morceau-admin',
  templateUrl: './morceau-admin.component.html',
  styleUrls: ['./morceau-admin.component.scss']
})
export class MorceauAdminComponent implements OnInit {
  @ViewChild('descriptionDialog') descriptionDialog!: TemplateRef<any>

  morceaux$!: Observable<Morceau[]>

  displayedColumns = [
    'name',
    'mp3',
    'description',
  ]

  constructor(private s: MorceauService,
      private dialog: MatDialog) {
    this.morceaux$ = s.getMorceaux$()
  }

  ngOnInit(): void {
  }

  create() {
    this.s.update({
      name: '',
      mp3: '',
      description: '',
    })
  }

  update(morceau: Morceau, change: keyof Morceau, val: string) {
    this.s.update({
      ...morceau,
      [change]: val,
    })
  }

  openDescription(m: Morceau) {
    this.dialog.open(this.descriptionDialog, {
      data: { m },
    })
  }

  trackBy(i: number, item: Morceau): any {
    return item.id || i
  }
}
