import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniMorceauComponent } from './mini-morceau.component';

describe('MiniMorceauComponent', () => {
  let component: MiniMorceauComponent;
  let fixture: ComponentFixture<MiniMorceauComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiniMorceauComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MiniMorceauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
