import { Component, OnInit } from '@angular/core';
import { Morceau } from 'functions/src/models/morceau';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MorceauService } from '../morceau/morceau.service';

@Component({
  selector: 'app-morceaux',
  templateUrl: './morceaux.component.html',
  styleUrls: ['./morceaux.component.scss']
})
export class MorceauxComponent implements OnInit {

  loading = true

  morceaux$: Observable<Morceau[]>

  constructor(s: MorceauService) {
    this.morceaux$ = s.getMorceaux$().pipe(tap(_ => {
      // Workaround to avoid "NG0100: Expression has changed after it was checked"
      setTimeout(() => this.loading = false, 0)
    }))
  }

  ngOnInit(): void {
  }

  trackBy(_: number, m: Morceau): any {
    return m.id
  }

}
