import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MorceauxComponent } from './morceaux.component';

const routes: Routes = [{
  path: ':id',
  loadChildren: () => import('../morceau-detail/morceau-detail.module').then(m => m.MorceauDetailModule)
}, {
  path: '',
  component: MorceauxComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MorceauxRoutingModule { }
