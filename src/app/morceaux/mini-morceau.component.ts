import { Component, Input, OnInit } from '@angular/core';
import { Morceau } from 'functions/src/models/morceau';

@Component({
  selector: 'app-mini-morceau',
  templateUrl: './mini-morceau.component.html',
  styleUrls: ['./mini-morceau.component.scss']
})
export class MiniMorceauComponent implements OnInit {

  @Input() morceau!: Morceau

  constructor() { }

  ngOnInit(): void {
  }

}
