import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    title: 'FIFA World Clique 2022',
  },
  {
    path: 'match',
    loadChildren: () => import('./match-detail/match-detail.module').then(m => m.MatchDetailModule),
  }, {
    path: 'matches',
    loadChildren: () => import('./matches-per-type/matches-per-type.module').then(m => m.MatchesPerTypeModule),
  }, {
    path: 'morceaux',
    loadChildren: () => import('./morceaux/morceaux.module').then(m => m.MorceauxModule),
  }, {
    path: 'groupes',
    loadChildren: () => import('./groups/groups.module').then(m => m.GroupsModule)
  }, {
    path: 'eliminations',
    loadChildren: () => import('./knockout/knockout.module').then(m => m.KnockoutModule)
  }, {
    path: 'infos',
    loadChildren: () => import('./infos/infos.module').then(m => m.InfosModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
  },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking',
    scrollPositionRestoration: 'enabled',
}),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
