import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { MatButtonModule } from '@angular/material/button';
import { GroupAdminModule } from '../group-admin/group-admin.module';
import { MorceauAdminModule } from '../morceau-admin/morceau-admin.module';


@NgModule({
  declarations: [
    AdminComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatButtonModule,
    GroupAdminModule,
    MorceauAdminModule,
  ]
})
export class AdminModule { }
