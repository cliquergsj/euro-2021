import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Match } from 'functions/src/models/match';
import { Observable } from 'rxjs';
import { MatchService } from './match.service';

@Injectable({
  providedIn: 'root'
})
export class MatchResolver implements Resolve<Match|undefined> {
  constructor(private s: MatchService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Match|undefined> {
    const id = route.paramMap.get('id')!
    return this.s.getMatch$(id)
  }
}
