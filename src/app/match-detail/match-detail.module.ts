import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatchDetailRoutingModule } from './match-detail-routing.module';
import { MatchDetailComponent } from './match-detail.component';
import { MatchComponent } from './match.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MorceauModule } from '../morceau/morceau.module';
import { GroupTableModule } from '../group-table/group-table.module';


@NgModule({
  declarations: [
    MatchDetailComponent,
    MatchComponent,
  ],
  imports: [
    CommonModule,
    MatchDetailRoutingModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MorceauModule,
    GroupTableModule,
  ]
})
export class MatchDetailModule { }
