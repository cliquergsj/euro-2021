import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupName } from 'functions/src/models/group';
import { Match } from 'functions/src/models/match';
import { Observable } from 'rxjs';
import { pluck, switchMap } from 'rxjs/operators';
import { MatchNameService } from '../matches/match-name.service';
import { MatchService } from './match.service';

type Data = { match: Match }

@Component({
  selector: 'app-match-detail',
  templateUrl: './match-detail.component.html',
  styleUrls: ['./match-detail.component.scss']
})
export class MatchDetailComponent implements OnInit {
  match!: Match

  next$: Observable<Match>
  prev$: Observable<Match>

  get group(): GroupName|undefined {
    switch (this.match.type) {
    case 'group1':
    case 'group2':
    case 'group3':
      return this.match.left?.id[0] as GroupName
    }
    return undefined
  }

  constructor(public typeServ: MatchNameService,
      serv: MatchService,
      activatedRoute: ActivatedRoute) {
    const match$ = (activatedRoute.data as Observable<Data>).pipe(pluck('match'))

    const next$ = match$.pipe(switchMap(m => serv.nextMatch$(m)))
    const prev$ = match$.pipe(switchMap(m => serv.previousMatch$(m)))

    match$.subscribe(m => this.match = m)
    this.next$ = next$
    this.prev$ = prev$
  }

  ngOnInit(): void {
  }

}
