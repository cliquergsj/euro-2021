import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { refEqual } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Match, TIE_DIFFERENCE } from 'functions/src/models/match';
import { ConnectableObservable, Subject } from 'rxjs';
import { map, publishReplay, switchMap, tap } from 'rxjs/operators';
import { UserService } from '../login/user.service';
import { MorceauService } from '../morceau/morceau.service';
import { MatchService } from './match.service';
import { VoteService } from './vote.service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnChanges {

  tieDiff = TIE_DIFFERENCE

  @Input('match') match!: Match
  private match$ = new Subject<Match>()

  loadingVote = false
  vote$ = this.match$.pipe(
      tap(_ => this.loadingVote = true),
      switchMap(m => this.voteServ.getVote$(m.id!)),
      tap(_ => this.loadingVote = false),
      map(vote => {
        if (!vote?.votedFor) {
          return
        }
        const left = this.match.left
        const right = this.match.right
        if (left && refEqual(vote.votedFor, left))
          return 'left'
        else if (right && refEqual(vote.votedFor, right))
          return 'right'
        return
      }),
      publishReplay(1),
    ) as ConnectableObservable<'left'|'right'|undefined>

  isVoteForLeft = this.vote$.pipe(map(v => v === 'left'))
  isVoteForRight = this.vote$.pipe(map(v => v === 'right'))

  get votable(): boolean {
    const m = this.match
    return !!m.left && !!m.right &&
      m.date.toDate() >= this.service.endOfMatch
  }

  constructor(public user: UserService,
      private service: MatchService,
      private voteServ: VoteService,
      public morceauServ: MorceauService,
      public router: Router) {
    this.vote$.connect()
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.match) {
      this.match$.next(this.match)
    }
  }

  voteFor(side?: 'left' | 'right') {
    if (!this.votable) {
      return
    }

    const morceau = side === undefined ?
      undefined : this.match[side]

    this.voteServ.voteFor(this.match.id!, morceau)
  }
}
