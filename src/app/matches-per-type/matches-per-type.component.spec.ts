import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchesPerTypeComponent } from './matches-per-type.component';

describe('MatchesPerTypeComponent', () => {
  let component: MatchesPerTypeComponent;
  let fixture: ComponentFixture<MatchesPerTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchesPerTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchesPerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
