import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchesPerTypeComponent } from './matches-per-type.component';
import { MatchesPerTypeRoutingModule } from './matches-per-type-routing.module';
import { MatchesModule } from '../matches/matches.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    MatchesPerTypeComponent,
  ],
  imports: [
    CommonModule,
    MatchesPerTypeRoutingModule,
    MatchesModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
  ]
})
export class MatchesPerTypeModule { }
