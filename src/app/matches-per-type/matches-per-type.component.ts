import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatchType } from 'functions/src/models/match';
import { Observable } from 'rxjs';
import { map, pluck } from 'rxjs/operators';
import { MatchNameService } from '../matches/match-name.service';

@Component({
  selector: 'app-matches-per-type',
  templateUrl: './matches-per-type.component.html',
  styleUrls: ['./matches-per-type.component.scss']
})
export class MatchesPerTypeComponent implements OnInit {

  readonly types: readonly MatchType[] = [
    'group1',
    'group2',
    'group3',
    'eighth',
    'quarter',
    'semi',
    'final',
  ]

  type$: Observable<MatchType>;
  prevType$: Observable<MatchType>;
  nextType$: Observable<MatchType>;

  constructor(public typeServ: MatchNameService,
      route: ActivatedRoute) {
    
    const type$ = route.firstChild!.params
      .pipe(pluck('type')) as Observable<MatchType>
    const idx$ = type$
      .pipe(map(t => this.types.indexOf(t)))
    const prevType$ = idx$
      .pipe(map(idx => this.types[idx - 1]))
    const nextType$ = idx$
      .pipe(map(idx => this.types[idx + 1]))

    this.type$ = type$
    this.prevType$ = prevType$
    this.nextType$ = nextType$
  }

  ngOnInit(): void {
  }

}
