import { Component, Input, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as md from 'markdown-it'
import { Morceau } from 'functions/src/models/morceau';
import { PlayerService } from '../player/player.service';

const DESCRIPTION_LIMIT_LOW = 75
const DESCRIPTION_LIMIT_HIGH = 95


@Component({
  selector: 'app-morceau',
  templateUrl: './morceau.component.html',
  styleUrls: ['./morceau.component.scss']
})
export class MorceauComponent {
  @Input() morceau?: Morceau | null = undefined

  @ViewChild('descriptionTemplate') descriptionTemplate!: TemplateRef<void>

  private md: md;

  get hasMore(): boolean {
    const lines = (this.morceau?.description || '').split('\n')
    return (lines.length !== 1 || lines[0].length > DESCRIPTION_LIMIT_HIGH)
  }

  get descriptionSample(): string {
    const lines = (this.morceau?.description || '').split('\n')

    const shortText = lines[0].substr(0, DESCRIPTION_LIMIT_LOW).split(' ')
    if (shortText.length > 1) {
      shortText.pop() // remove last word to avoid cut words
    }
    return `${shortText.join(' ')}…`
  }

  get mdDescription(): string {
    return this.md.render(this.morceau?.description || '')
  }

  get playing(): boolean {
    return this.player.isPlaying(this.morceau!)
  }

  constructor(private player: PlayerService,
    private dialog: MatDialog) {
    this.md = md()
  }

  togglePlay() {
    if (this.playing) {
      this.player.pause()
    } else {
      this.player.play(this.morceau!)
    }
  }
  
  readMore() {
    this.dialog.open(this.descriptionTemplate)
  }
}
