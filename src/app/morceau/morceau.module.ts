import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MorceauComponent } from './morceau.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    MorceauComponent,
  ],
  exports: [MorceauComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
  ],
})
export class MorceauModule { }
