import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InfosRoutingModule } from './infos-routing.module';
import { InfosComponent } from './infos.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';


@NgModule({
  declarations: [
    InfosComponent
  ],
  imports: [
    CommonModule,
    InfosRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
  ]
})
export class InfosModule { }
