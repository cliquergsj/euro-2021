import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupTableComponent } from './group-table.component';
import { MatTableModule } from '@angular/material/table';



@NgModule({
  declarations: [
    GroupTableComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
  ],
  exports: [
    GroupTableComponent,
  ]
})
export class GroupTableModule { }
