import { Component, Input, OnInit } from '@angular/core';
import { GroupMember, GroupName } from 'functions/src/models/group';
import { Observable, of } from 'rxjs';
import { MorceauService } from '../morceau/morceau.service';
import { GroupService } from './group.service';


@Component({
  selector: 'app-group-table',
  templateUrl: './group-table.component.html',
  styleUrls: ['./group-table.component.scss']
})
export class GroupTableComponent implements OnInit {
  @Input() group!: GroupName

  members$: Observable<GroupMember[]> = of([])

  displayedColumns = [
    'name',
    'points',
    'played',
    'won',
    'drawn',
    'lose',
    'goalsFor',
    'goalsAgainst',
    'goalsDifference',
  ]

  constructor(private service: GroupService,
      public morceauServ: MorceauService) {
  }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.members$ = this.service.getMembersOfGroup$(this.group)
  }

  trackBy(_: number, item: GroupMember): any {
    return item.id
  }
}
