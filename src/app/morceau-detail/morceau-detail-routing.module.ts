import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MorceauDetailComponent } from './morceau-detail.component';
import { MorceauResolver } from './morceau.resolver';

const routes: Routes = [{
  path: '',
  component: MorceauDetailComponent,
  resolve: {
    morceau: MorceauResolver,
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MorceauDetailRoutingModule { }
