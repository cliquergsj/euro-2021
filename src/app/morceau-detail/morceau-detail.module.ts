import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MorceauDetailRoutingModule } from './morceau-detail-routing.module';
import { MorceauDetailComponent } from './morceau-detail.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    MorceauDetailComponent
  ],
  imports: [
    CommonModule,
    MorceauDetailRoutingModule,
    MatButtonModule,
    MatIconModule,
  ]
})
export class MorceauDetailModule { }
