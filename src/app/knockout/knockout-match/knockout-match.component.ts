import { Component, Input, OnInit } from '@angular/core';
import { Match } from 'functions/src/models/match';
import { MorceauService } from '../../morceau/morceau.service';

@Component({
  selector: 'app-knockout-match',
  templateUrl: './knockout-match.component.html',
  styleUrls: ['./knockout-match.component.scss']
})
export class KnockoutMatchComponent implements OnInit {

  @Input() match!: Match

  constructor(public morceauServ: MorceauService) { }

  ngOnInit(): void {
  }

}
