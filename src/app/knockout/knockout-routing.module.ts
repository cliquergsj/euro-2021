import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KnockoutComponent } from './knockout.component';

const routes: Routes = [{
  path: '',
  component: KnockoutComponent,
  title: 'Phase à élimination directe · FIFA World Clique 2022',
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KnockoutRoutingModule { }
