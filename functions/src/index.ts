import {https, pubsub} from "firebase-functions";
import {initializeApp} from "firebase-admin/app";
import {endMatches as em} from "./end-match";

initializeApp();

export const endMatches = https.onCall(async (_data, ctx) => {
  const token = ctx.auth?.token;
  if (!token || token.admin !== "2022") {
    return false;
  }
  await em();

  return true;
});

export const endMatchesSchedule = pubsub
    .schedule("45 * * * *")
    .onRun(async () => {
      await em();
    });
