import {DocumentReference} from "firebase/firestore";
import {GroupMember} from "./group";

export const VOTES_COLLECTION = "votes";

export interface Vote {
  votedFor: DocumentReference<GroupMember>
}
