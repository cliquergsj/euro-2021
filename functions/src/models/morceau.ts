export const MORCEAUX_COLLECTION = "morceaux";

export interface Morceau {
  id?: string
  name: string
  mp3?: string
  description?: string
  handicap?: number
}
