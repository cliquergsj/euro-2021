import {Morceau} from "./morceau";
import {DocumentReference} from "firebase/firestore";

export const GROUPS_COLLECTION = "groups";

export type GroupName = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H"

export interface GroupMember {
  id?: string
  group: GroupName
  morceau?: DocumentReference<Morceau>

  played: number
  won: number
  drawn: number
  lose: number
  goalsFor: number
  goalsAgainst: number
  goalsDifference: number
  points: number

  qualified?: boolean
  eliminated?: boolean
}
